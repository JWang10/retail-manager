﻿using System.Collections.Generic;

namespace JRMDesktopUI
{
    public interface ICalculation
    {
        List<string> Register { get; set; }

        double Add(double x, double y);
    }
}